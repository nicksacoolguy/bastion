﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

    [Header("Set in inspector")]
    public ObjectPooler objectPooler;
    public Transform spawnPoint;
    public int numberOfCars;
    public int distance;
	private void OnEnable()
	{
        DebugInput.OnRestart += HandleRestart; 
	}

	private void OnDisable()
	{
        DebugInput.OnRestart -= HandleRestart;		
	}

	private void HandleRestart()
	{
        StartCoroutine(CreateCars(numberOfCars));
	}

	public void Reset(Car car)
	{
        GameObject carGo = car.gameObject;
        carGo.transform.position = new Vector3(Random.Range(spawnPoint.position.x - 30, spawnPoint.position.x + 30), spawnPoint.position.y, Random.Range(spawnPoint.position.z - 30, spawnPoint.position.z + 30));
	}

	public IEnumerator CreateCars(int numOfCars){

        GameObject carGo = objectPooler.GetPooledObject("Car");
		carGo.transform.position = new Vector3(Random.Range(spawnPoint.position.x - 30, spawnPoint.position.x + 30), spawnPoint.position.y, Random.Range(spawnPoint.position.z - 30, spawnPoint.position.z + 30));
        carGo.GetComponent<Car>().carController = this;
        carGo.SetActive(true);
        //GameObject carGo = Resources.Load<GameObject>("Car");
        //carGo = Instantiate(carGo, new Vector3(Random.Range(spawnPoint.position.x -30, spawnPoint.position.x + 30), spawnPoint.position.y, Random.Range(spawnPoint.position.z -30, spawnPoint.position.z +30)), spawnPoint.rotation);
        yield return null;
    }
}
