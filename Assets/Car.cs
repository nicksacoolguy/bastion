﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {
	
    [Header("Initialized by script")]
	public Transform target;
    public CarController carController;
    [Header("Set in the inspector")]
    public Rigidbody rb = null;
    public Transform steering;
    public Transform driver;
    public float force = 15;
    public float topSpeed = 15;

    [Header("Set at runtime")]
    public bool allClear = true;
    public float speed = 0;
    public float upright = 0;
    public float distance = 0;
    public float steerDamping = 0;

    private bool buffer;

	void Start () {
		target = GameObject.FindGameObjectWithTag("Target").transform;
	}

	// Update is called once per frame
	private void Update()
	{
        driver.LookAt(target);
        speed = rb.velocity.magnitude;
        distance = Vector3.Distance(this.transform.position, target.transform.position);
        upright = Quaternion.Angle(Quaternion.Euler(this.transform.up), Quaternion.Euler(Vector3.up));

        if (upright > 0.5f || distance < 5)
        {
            allClear = false;
            if(!buffer)
                StartCoroutine(ResetBuffer());
        }
        else
        {

            allClear = true;
        }
	}

	private void OnTriggerEnter(Collider other)
	{
        if(other.gameObject.name == "Cube")
            carController.Reset(this);
	}

	IEnumerator ResetBuffer(){
        buffer = true;
        yield return new WaitForSeconds(5f);
        if(!allClear){
            carController.Reset(this);
        }
        buffer = false; 
        yield return null;

    }

	void FixedUpdate () {

        Drive();
    }

    void Drive(){

        Steer();

        if(allClear && speed < topSpeed){
            Accelerate();
        }
    }

	void Accelerate(){

        if(speed < 5){
            rb.AddRelativeForce(steering.forward * force, ForceMode.Force);
        }else{
            
			rb.AddRelativeForce(steering.forward * force, ForceMode.Force);
        }

	}

    void Steer(){

        if(allClear){
            steering.rotation = Quaternion.RotateTowards(steering.rotation, driver.rotation, 1f);
            steerDamping = Quaternion.Angle(steering.rotation, driver.rotation);
            if (steerDamping > 1 && steerDamping < 45){
                
				rb.MoveRotation(steering.rotation);
            }
        }
    }

	void Brake(){
		
	}

    void OnDrawGizmos(){
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(steering.position, steering.forward);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(steering.position, driver.forward);
        Gizmos.DrawRay(this.transform.position, this.transform.up);
    }
}
