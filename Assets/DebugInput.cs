﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInput : MonoBehaviour {

    public delegate void Restart();
    public static event Restart OnRestart;

	
	// Update is called once per frame
	void Update () {

        OnRestart();


        if(Input.GetKeyDown(KeyCode.R)){

            if(OnRestart != null){
                OnRestart();
            }
        }
	}
}
